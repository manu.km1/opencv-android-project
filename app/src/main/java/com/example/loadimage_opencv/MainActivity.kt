package com.example.loadimage_opencv

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import com.example.loadimage_opencv.databinding.ActivityMainBinding
import org.opencv.android.CameraActivity
import org.opencv.android.CameraBridgeViewBase
import org.opencv.android.OpenCVLoader
import org.opencv.core.Mat
import org.opencv.core.MatOfPoint
import org.opencv.core.MatOfPoint2f
import org.opencv.core.Point
import org.opencv.core.Scalar
import org.opencv.core.Size
import org.opencv.imgproc.Imgproc

class MainActivity : CameraActivity() {
    private lateinit var binding : ActivityMainBinding

    private  lateinit var cameraBridgeViewBase: CameraBridgeViewBase
    private lateinit var image : Mat
    private lateinit var gray : Mat
    private lateinit var blur : Mat
    private lateinit var canny : Mat
    private lateinit var threshold : Mat
    private lateinit var hierarchy : Mat


    val WIDTH = 800.0
    val HEIGHT = 600.0


    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getPermission()
        cameraBridgeViewBase = binding.cameraView
        cameraBridgeViewBase.setCvCameraViewListener(object : CameraBridgeViewBase.CvCameraViewListener2{

            override fun onCameraViewStarted(width: Int, height: Int) {
                image = Mat()
                gray = Mat()
                blur = Mat()
                canny = Mat()
                threshold = Mat()
                hierarchy = Mat()
            }

            override fun onCameraViewStopped() {
                image.release()
                gray.release()
                blur.release()
                canny.release()
                threshold.release()
                hierarchy.release()

            }

            override fun onCameraFrame(inputFrame: CameraBridgeViewBase.CvCameraViewFrame?): Mat {
                image = inputFrame!!.rgba() // Original Camera Image

                val documentContour = MatOfPoint(MatOfPoint(Point(0.0, 0.0), Point(0.0, 0.0), Point(0.0, 0.0), Point(0.0, 0.0)))
                Imgproc.cvtColor(image, gray, Imgproc.COLOR_BGR2GRAY)
                Imgproc.GaussianBlur(gray, blur, Size(9.0, 9.0), 4.0,4.0)
                Imgproc.Canny(blur, canny, 200.0, 300.0, 5, true)
                val contours = mutableListOf<MatOfPoint>()
                Imgproc.findContours(canny, contours, hierarchy, Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE)
                contours.sortByDescending { Imgproc.contourArea(it) }
                var maxArea = 0.0
                var maxContour: MatOfPoint? = null
                for (contour in contours) {
                    val area = Imgproc.contourArea(contour)
                    if (area > 2000) {
                        val peri = Imgproc.arcLength(MatOfPoint2f(*contour.toArray()), true)
                        val approx = MatOfPoint2f()
                        Imgproc.approxPolyDP(MatOfPoint2f(*contour.toArray()), approx, 0.015 * peri, true)
                        if (area > maxArea && approx.toArray().size >= 4) {
                            maxArea = area
                            maxContour = contour
                        }
                    }
                }
                maxContour?.let {
                    val documentContourArray = it.toArray()
                    val documentContourList = mutableListOf<Point>()
                    for (point in documentContourArray) {
                        documentContourList.add(Point(point.x, point.y))
                    }
                    documentContour.fromList(documentContourList)
                    Imgproc.drawContours(image, listOf(documentContour), -1, Scalar(0.0, 255.0, 0.0), 2)
                }

                return image // Returning processed image
            }
        })
        if(OpenCVLoader.initDebug()) {
            Log.d("MANU", "---------------------OpenCV is Working-------------------------")
            cameraBridgeViewBase.enableView()
        }
    }


    override fun onResume() {
        super.onResume()
        cameraBridgeViewBase.enableView()
    }

    override fun onDestroy() {
        super.onDestroy()
        cameraBridgeViewBase.disableView()
    }

    override fun onPause() {
        super.onPause()
        cameraBridgeViewBase.disableView()
    }

    override fun getCameraViewList(): MutableList<out CameraBridgeViewBase> {
        return listOf(cameraBridgeViewBase).toMutableList()
    }

    private fun getPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(checkSelfPermission(Manifest.permission.CAMERA)!= PackageManager.PERMISSION_GRANTED){
                requestPermissions(arrayOf(Manifest.permission.CAMERA),101)
            }
        }
    }
    override fun onRequestPermissionsResult(requestCode: Int,permissions: Array<out String>,grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(grantResults.size>0 && grantResults[0]!=PackageManager.PERMISSION_GRANTED){
            getPermission()
        }
    }

}